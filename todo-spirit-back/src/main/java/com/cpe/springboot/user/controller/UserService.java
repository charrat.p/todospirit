package com.cpe.springboot.user.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.cpe.springboot.user.model.UserConnected;
import com.cpe.springboot.user.model.UserModel;

@Service
public class UserService {

	private final UserRepository userRepository;

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public List<UserModel> getAllUsers() {
		List<UserModel> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}

	public Optional<UserModel> getUser(String id) {
		return userRepository.findById(Integer.valueOf(id));
	}

	public UserModel getUser(Integer id) {
		UserModel user = null;
		Optional<UserModel> uOpt = userRepository.findById(id);
		if (uOpt.isPresent()) {
			user = uOpt.get();
		}
		return user;
	}

	public boolean addUser(String login,String pwd,String mail) {
		UserModel u = new UserModel(login,pwd,mail);
		userRepository.save(u);
		return true;
	}

	public void addASpiritControl(int idPlayer,int idSpirit) {
		Optional<UserModel> u = userRepository.findById(idPlayer);
		if(u.isPresent()) {
			UserModel user = u.get();
			user.setIdSpirit(idSpirit);
			userRepository.save(user);
		}		
	}

	public void addAnObjectif(int idPlayer) {
		Optional<UserModel> u = userRepository.findById(idPlayer);
		if(u.isPresent()) {
			UserModel user = u.get();
			//TODO gérer l'ajout d'un objectif
			userRepository.save(user);
		}		
	}

	public List<UserModel> getUserByLoginPwd(String login, String pwd) {
		List<UserModel> ulist = null;
		ulist = userRepository.findByLoginAndPwd(login, pwd);
		return ulist;
	}
	
	public Integer addPoints(int idPlayer, int points) {
		Integer ret = -1;
		Optional<UserModel> u = userRepository.findById(idPlayer);
		if(u.isPresent()) {
			UserModel user = u.get();
			user.addPoints(points);
			userRepository.save(user);
			ret = user.getPoints();
		}
		return ret;
	}
	
	public void subPoints(int idPlayer, int points) {
		Optional<UserModel> u = userRepository.findById(idPlayer);
		if(u.isPresent()) {
			UserModel user = u.get();
			user.subPoints(points);
			userRepository.save(user);
		}		
	}

	public Boolean followAnewChallenge(Integer idPlayer, Integer idChall) {
		Boolean ret = false;
		Optional<UserModel> uOpt = userRepository.findById(idPlayer);
		if ( uOpt.isPresent() ) {
			UserModel user = uOpt.get();
			for (int i=0; i < user.getChallengesFollow().size();i++) {
				System.out.println(user.getChallengesFollow().get(i));
				if (user.getChallengesFollow().get(i) == idChall) {
					return ret;
				}
			}
			user.followANewChallenge(idChall);
			userRepository.save(user);
			ret = true;
		}
		return ret;
	}

	public UserConnected connexion(String login, String pwd) {
		UserConnected ret = null;
		List<UserModel> uOpt = userRepository.findByLoginAndPwd(login,pwd);
		if ( uOpt.size() > 0 ) {
			UserModel user = uOpt.get(0);
			ret = new UserConnected(user.getId(),login,user.getChallengesFollow(),user.getPoints(),user.getIdSpirit(),user.isNewest());
		}
		return ret;
	}
	
	public void changeTheStatutOfUser(Integer idPlayer) {
		Optional<UserModel> uOpt = userRepository.findById(idPlayer);
		if ( uOpt.isPresent() ) {
			UserModel user = uOpt.get();
			user.setNewest(false);
			userRepository.save(user);
		}		
	}

	public Boolean removeChallenge(Integer idChall, Integer idUser) {
		Boolean ret = false;
		Optional<UserModel> uOpt = userRepository.findById(idUser);
		if ( uOpt.isPresent() ) {
			UserModel user = uOpt.get();
			ArrayList<Integer> arr_new = new ArrayList<Integer>();
	        for( int i=0;i<user.getChallenges().size();i++) {
	        	int idChallTest = user.getChallenges().get(i);
	            if(idChallTest != idChall){
	                arr_new.add(idChallTest);
	            }
	        }
	        ret = true;
	        user.setChallengesFollow(arr_new);
			userRepository.save(user);
		}		
		return ret;
	}
}
