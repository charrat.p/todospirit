package com.cpe.springboot.user.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class UserModel implements Serializable {

	private static final long serialVersionUID = 2733795832476568049L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String login;
	private String pwd;
	private String email;
	private int points;
	private ArrayList<Integer> idchallengesFollow = new ArrayList<Integer>();
	private int idSpirit = -1;
	private boolean newest = true;
	
	public UserModel() {
		this.login = "";
		this.pwd = "";
		this.email = "";
	}

	public UserModel(String login, String pwd, String mail) {
		super();
		this.login = login;
		this.pwd = pwd;
		this.email = "email_default";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public void addPoints(int points) {
		this.points += points;
	}

	public void subPoints(int points) {
		this.points -= points;
	}

	public ArrayList<Integer> getChallenges() {
		return this.idchallengesFollow;
	}

	public void followANewChallenge(Integer idchall) {
		this.idchallengesFollow.add(idchall);
	}

	public boolean followTheChallenge(Integer idchall) {
		boolean ret = false;
		for (int i = 0; i < this.idchallengesFollow.size(); i++) {
			Integer challengeFollow = this.idchallengesFollow.get(i);
			if (challengeFollow.equals(idchall)) {
				ret = true;
			}
		}
		return ret;
	}

	public List<Integer> getChallengesFollow() {
		return idchallengesFollow;
	}

	public void setChallengesFollow(ArrayList<Integer> challengesFollow) {
		this.idchallengesFollow = challengesFollow;
	}

	public int getIdSpirit() {
		return idSpirit;
	}

	public void setIdSpirit(int idSpirit) {
		this.idSpirit = idSpirit;
	}

	public ArrayList<Integer> getIdchallengesFollow() {
		return idchallengesFollow;
	}

	public void setIdchallengesFollow(ArrayList<Integer> idchallengesFollow) {
		this.idchallengesFollow = idchallengesFollow;
	}

	public boolean isNewest() {
		return newest;
	}

	public void setNewest(boolean newest) {
		this.newest = newest;
	}
}