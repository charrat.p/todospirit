package com.cpe.springboot.user.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.cpe.springboot.user.model.UserConnected;
import com.cpe.springboot.user.model.UserConnexion;
import com.cpe.springboot.user.model.UserModel;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class UserRestController {
	private final UserService userService;
	
	public UserRestController(UserService userService) {
		this.userService=userService;
	}
	
	@RequestMapping("/users")
	private List<UserModel> getAllUsers() {
		return userService.getAllUsers();

	}
	
	@RequestMapping("/user/{id}")
	private UserModel getUser(@PathVariable String id) {
		Optional<UserModel> ruser;
		ruser= userService.getUser(id);
		if(ruser.isPresent()) {
			return ruser.get();
		}
		return null;
	}
	
	@RequestMapping("/rank")
	private List<UserModel> getRank() {
		//TODO sort player by points
		return userService.getAllUsers();
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/user")
	public boolean addUser(@RequestBody UserConnexion user) {
		System.out.println(user.login);
		return userService.addUser(user.login,user.pwd,user.mail);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/user/{id}/challenges")
	public Boolean addUserChallenge(@PathVariable String id,@RequestBody String idChall) {
		return userService.followAnewChallenge(Integer.valueOf(id),Integer.valueOf(idChall));	
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/user/{id}/challenges")
	public Boolean removeUserChallenge(@PathVariable String id,@RequestBody String idChall) {
		return userService.removeChallenge(Integer.valueOf(idChall),Integer.valueOf(id));	
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/user/{id}/oldest")
	public void changeTheStatutOfUser(@PathVariable String id) {
		userService.changeTheStatutOfUser(Integer.valueOf(id)
);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/user/{id}/spirit")
	public void addUserSpirit(@PathVariable String id,@RequestBody String idSpirit) {
		userService.addASpiritControl(Integer.valueOf(id),Integer.valueOf(idSpirit));
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/connexion")
	public UserConnected connexion(@RequestBody UserConnexion userConnexion) {
		return userService.connexion(userConnexion.login,userConnexion.pwd);
	}
}
