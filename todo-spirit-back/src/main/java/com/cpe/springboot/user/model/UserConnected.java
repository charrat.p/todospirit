package com.cpe.springboot.user.model;

import java.util.List;

public class UserConnected {
	public Integer id;
	public String login;
	public List<Integer> idchallengesFollow;
	public Integer points;
	public Integer idSpirit;
	public Boolean newest;
	
	public UserConnected(Integer id, String login, List<Integer> idchallengesFollow, Integer points, Integer idSpirit, Boolean newest) {
		super();
		this.id = id;
		this.login = login;
		this.idchallengesFollow = idchallengesFollow;
		this.points = points;
		this.idSpirit = idSpirit;
		this.newest = newest;
	}
	
	
}
