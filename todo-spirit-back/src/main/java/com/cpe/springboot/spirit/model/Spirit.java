package com.cpe.springboot.spirit.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Spirit implements Serializable {

	private static final long serialVersionUID = 2733795832476568049L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private Integer idPlayer;
	private Integer idTemplate;
	private String name;
	private String urlImg;
	private SpiritType type;
	private Integer attack;
	private Integer life;
	private Integer stamina;
	private Integer xp;
	private Integer xpToNextLevel;
	private Integer level;
	private Integer evolution;
	private String nickname;

	public Spirit() {
		super();
		this.idPlayer = 0;
		this.idTemplate = 0;
		this.name = "";
		this.urlImg = "";
		this.type = SpiritType.HEROIQUE;
		this.attack = 0;
		this.life = 0;
		this.stamina = 0;
		this.xp = 0;
		this.xpToNextLevel = 0;
		this.level = 0;
		this.evolution = 0;
		this.nickname = "";
	}

	public Spirit(Integer idPlayer,SpiritTemplate spiritTemplate) {
		super();
		this.idPlayer = idPlayer;
		this.idTemplate = spiritTemplate.getId();
		this.name = spiritTemplate.getName();
		this.urlImg = spiritTemplate.getUrlImg();
		this.type = spiritTemplate.getType();
		this.attack = spiritTemplate.getAttack();
		this.life = spiritTemplate.getLife();
		this.stamina = spiritTemplate.getStamina();
		this.evolution = spiritTemplate.getEvolution();
		this.xp = 0;
		this.xpToNextLevel = 100;
		this.level = 1;
		this.nickname = "";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrlImg() {
		return urlImg;
	}

	public void setUrlImg(String urlImg) {
		this.urlImg = urlImg;
	}

	public SpiritType getType() {
		return type;
	}

	public void setType(SpiritType type) {
		this.type = type;
	}

	public Integer getAttack() {
		return attack;
	}

	public void setAttack(Integer attack) {
		this.attack = attack;
	}

	public Integer getLife() {
		return life;
	}

	public void setLife(Integer life) {
		this.life = life;
	}

	public Integer getStamina() {
		return stamina;
	}

	public void setStamina(Integer stamina) {
		this.stamina = stamina;
	}

	public Integer getIdPlayer() {
		return idPlayer;
	}

	public void setIdPlayer(Integer idPlayer) {
		this.idPlayer = idPlayer;
	}

	public Integer getIdTemplate() {
		return idTemplate;
	}

	public void setIdTemplate(Integer idTemplate) {
		this.idTemplate = idTemplate;
	}

	public Integer getXp() {
		return xp;
	}

	public void setXp(Integer xp) {
		this.xp = xp;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getXpToNextLevel() {
		return xpToNextLevel;
	}

	public void setXpToNextLevel(Integer xpToNextLevel) {
		this.xpToNextLevel = xpToNextLevel;
	}	
	
	public void winXP(Integer xp) {
		this.xp += xp;
	}
	
	public boolean canlevelUp() {
		return this.xpToNextLevel != -1 &&
				this.xpToNextLevel <= this.xp;
	}
	
	public void levelUp() {
		this.xp = 0;
		this.level += 1;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Integer getEvolution() {
		return evolution;
	}

	public void setEvolution(Integer evolution) {
		this.evolution = evolution;
	}
}
