package com.cpe.springboot.spirit.controller;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.spirit.model.Spirit;
import com.cpe.springboot.spirit.model.SpiritTemplate;
import com.cpe.springboot.user.model.UserModel;

public interface SpiritTemplateRepository extends CrudRepository<SpiritTemplate, Integer> {

}
