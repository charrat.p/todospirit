package com.cpe.springboot.spirit.controller;

import java.util.List;
import org.springframework.web.bind.annotation.*;

import com.cpe.springboot.spirit.model.Spirit;
import com.cpe.springboot.spirit.model.SpiritTemplate;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class SpiritController {
	private final SpiritService spiritService;
	private final SpiritTemplateService spiritTemplateService;
	
	public SpiritController(SpiritService spiritService, SpiritTemplateService spiritTemplateService) {
		this.spiritService=spiritService;
		this.spiritTemplateService = spiritTemplateService;
	}
	
	@RequestMapping("/spirits")
	private List<SpiritTemplate> getAllSpiritsTemplate() {
		return spiritTemplateService.getAllSpiritTemplate();
	}
	
	@RequestMapping("/spirits/controlled")
	private List<Spirit> getAllSpirit() {
		return spiritService.getAllSpirit();
	}
	
	@RequestMapping("/spirits/detection")
	private List<SpiritTemplate> doAdectection() {
		return spiritTemplateService.doAdetection();
	}
	
	@RequestMapping(value="/spirits/{id}")
	public Spirit getSpiritById(@PathVariable String id) {
		return spiritService.getSpiritById(Integer.valueOf(id));
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/spirits/{id}")
	public Boolean changeNickname(@PathVariable String id, @RequestParam String nickname) {
		return spiritService.changeNickname(Integer.valueOf(id),nickname);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/spirits/{id}")
	public Integer addAnewSpirit(@PathVariable String id,@RequestBody String iduser) {
		return spiritService.userControlANewSpirit(Integer.valueOf(iduser), Integer.valueOf(id));
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/spirits/{id}/evolution")
	public Spirit doEvolASpirit(@PathVariable String id,@RequestBody String iduser) {
		return spiritService.userDoEvoluatSpirit(Integer.valueOf(id), Integer.valueOf(iduser));
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/spirits/evolutiontest")
	public Spirit doEvolASpirit() {
		return spiritService.getSpiritById(79);
	}
	
	
}