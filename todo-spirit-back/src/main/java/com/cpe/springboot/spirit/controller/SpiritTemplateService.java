package com.cpe.springboot.spirit.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.cpe.springboot.spirit.model.SpiritTemplate;
import com.cpe.springboot.spirit.model.SpiritType;
import com.cpe.springboot.user.controller.UserService;

@Service
public class SpiritTemplateService {
	private final SpiritTemplateRepository spiritTemplateRepository;

	public SpiritTemplateService(SpiritTemplateRepository spiritTemplateRepository) {
		this.spiritTemplateRepository = spiritTemplateRepository;
	}
	
	public List<SpiritTemplate> getAllSpiritTemplate() {
		List<SpiritTemplate> spiritTemplateList = new ArrayList<>();
		spiritTemplateRepository.findAll().forEach(spiritTemplateList::add);
		return spiritTemplateList;
	}

	public SpiritTemplate getSpiritTemplateById(Integer id) {
		SpiritTemplate ret = null;
		Optional<SpiritTemplate> sOpt = spiritTemplateRepository.findById(id);
		if (sOpt != null) {
			ret = sOpt.get();
		}
		return ret;
	}
	
	public SpiritTemplate findARandom() {
		List<SpiritTemplate> spiritTemplateList = new ArrayList<>();
		spiritTemplateRepository.findAll().forEach(spiritTemplateList::add);
		Random rand = new Random();
	    return spiritTemplateList.get(rand.nextInt(spiritTemplateList.size()-1));
	}
	
	public void addSpirit(String name, String urlImg,SpiritType type, Integer attack, Integer life, Integer stamina, Integer evolution) {
		spiritTemplateRepository.save(new SpiritTemplate(name,urlImg,type,attack,life,stamina, evolution));  
	}	

	public List<SpiritTemplate> doAdetection() {
		ArrayList<SpiritTemplate> detection = new ArrayList<SpiritTemplate>();
		detection.add(findARandom());
		detection.add(findARandom());
		return detection;
	}
}