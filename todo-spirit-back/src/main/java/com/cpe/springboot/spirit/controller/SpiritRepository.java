package com.cpe.springboot.spirit.controller;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import com.cpe.springboot.spirit.model.Spirit;

public interface SpiritRepository extends CrudRepository<Spirit, Integer> {
	Optional<Spirit> findByIdAndIdPlayer(Integer id, Integer idPlayer);
}
