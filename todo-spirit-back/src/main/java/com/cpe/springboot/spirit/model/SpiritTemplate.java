package com.cpe.springboot.spirit.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class SpiritTemplate implements Serializable {

	private static final long serialVersionUID = 2733795832476568049L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	private String urlImg;
	private SpiritType type;
	private Integer attack;
	private Integer life;
	private Integer stamina;
	private Integer evolution;
	
	public SpiritTemplate() {
		super();
		this.name = "";
		this.urlImg = "";
		this.type = SpiritType.HEROIQUE;
		this.attack = 0;
		this.life = 0;
		this.stamina = 0;
		this.evolution = 0;
	}

	public SpiritTemplate(String name, String urlImg, SpiritType type, Integer attack, Integer life, Integer stamina, Integer evolution) {
		super();
		this.name = name;
		this.urlImg = urlImg;
		this.type = type;
		this.attack = attack;
		this.life = life;
		this.stamina = stamina;
		this.evolution = evolution;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrlImg() {
		return urlImg;
	}

	public void setUrlImg(String urlImg) {
		this.urlImg = urlImg;
	}

	public SpiritType getType() {
		return type;
	}

	public void setType(SpiritType type) {
		this.type = type;
	}

	public Integer getAttack() {
		return attack;
	}

	public void setAttack(Integer attack) {
		this.attack = attack;
	}

	public Integer getLife() {
		return life;
	}

	public void setLife(Integer life) {
		this.life = life;
	}

	public Integer getStamina() {
		return stamina;
	}

	public void setStamina(Integer stamina) {
		this.stamina = stamina;
	}

	public Integer getEvolution() {
		return evolution;
	}

	public void setEvolution(Integer evolution) {
		this.evolution = evolution;
	}
}
