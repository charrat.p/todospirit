package com.cpe.springboot.spirit.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.cpe.springboot.spirit.model.Spirit;
import com.cpe.springboot.spirit.model.SpiritTemplate;
import com.cpe.springboot.user.controller.UserService;

@Service
public class SpiritService {
	private final SpiritRepository spiritRepository;
	private final UserService userService;
	private final SpiritTemplateService spiritTemplateService;

	public SpiritService(SpiritRepository spiritRepository, UserService userService, SpiritTemplateService spiritTemplateService) {
		this.spiritRepository = spiritRepository;
		this.userService = userService;
		this.spiritTemplateService = spiritTemplateService;
	}

	public List<Spirit> getAllSpirit() {
		List<Spirit> spiritList = new ArrayList<>();
		spiritRepository.findAll().forEach(spiritList::add);
		return spiritList;
	}

	public Spirit getSpiritById(Integer id) {
		Spirit ret = null;
		Optional<Spirit> sOpt = spiritRepository.findById(id);
		if (sOpt.isPresent()) {
			ret = sOpt.get();
		}
		return ret;
	}

	public Integer userControlANewSpirit(Integer idUser, Integer idSpirit) {
		Integer ret = -1;
		SpiritTemplate spiritControll = spiritTemplateService.getSpiritTemplateById(idSpirit);
		Spirit spirit = new Spirit(idUser, spiritControll);
		spiritRepository.save(spirit);
		userService.addASpiritControl(idUser, spirit.getId());
		ret = spirit.getId();
		return ret;
	}

	public Integer spiritWinXp(int idSpirit, int xp) {
		Integer ret = 0;
		Optional<Spirit> sOpt = spiritRepository.findById(idSpirit);
		if (sOpt.isPresent()) {
			Spirit spirit = sOpt.get();
			spirit.winXP(xp);
			ret = spirit.getXp();
			spiritRepository.save(spirit);
		}
		return ret;
	}
	
	public Boolean changeNickname(int idSpirit, String nickname) {
		Boolean ret = false;
		Optional<Spirit> sOpt = spiritRepository.findById(idSpirit);
		if (sOpt.isPresent()) {
			Spirit spirit = sOpt.get();
			spirit.setNickname(nickname);
			spiritRepository.save(spirit);
			ret = true;
		}
		return ret;
	}

	public Spirit userDoEvoluatSpirit(Integer idSpirit, Integer idUser) {
		Spirit ret = null;
		Optional<Spirit> sOpt = spiritRepository.findByIdAndIdPlayer(idSpirit,idUser);
		System.out.println(sOpt.isPresent());
		if (sOpt.isPresent()) {
			Spirit spirit = sOpt.get();
			Integer newId = userControlANewSpirit(idUser,spirit.getEvolution());
			if (!spirit.getNickname().equals("")) {
				changeNickname(newId, spirit.getNickname());
			}
			ret = getSpiritById(newId);
		}
		return ret;
	}
}
