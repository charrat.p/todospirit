package com.cpe.springboot.challenge.controller;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.challenge.model.Challenge;
import com.cpe.springboot.challenge.model.ChallengeCategory;
import com.cpe.springboot.challenge.model.ChallengeDone;
import com.cpe.springboot.user.model.UserModel;

public interface ChallengeDoneRepository extends CrudRepository<ChallengeDone, Integer> {
	List<ChallengeDone> findByUser(Integer user);
	List<ChallengeDone> findByUserAndChall(Integer user, Integer chall);
}
