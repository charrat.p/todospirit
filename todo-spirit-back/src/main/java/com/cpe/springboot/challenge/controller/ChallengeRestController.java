package com.cpe.springboot.challenge.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.*;

import com.cpe.springboot.challenge.model.Challenge;
import com.cpe.springboot.challenge.model.ChallengeDoneDTO;
import com.cpe.springboot.challenge.model.ChallengeStatus;
import com.cpe.springboot.challenge.model.DoChallenge;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class ChallengeRestController {
	private final ChallengeService challengeService;
	private final ChallengeDoneService challengeDoneService;
	
	public ChallengeRestController(ChallengeService challengeService, ChallengeDoneService challengeDoneService) {
		this.challengeService=challengeService;
		this.challengeDoneService=challengeDoneService;
	}
	
	@RequestMapping("/challenges")
	private List<Challenge> getAllChallenges() {
		return challengeService.getAllChallenges();
	}
	
	@RequestMapping("/challenge/{id}")
	private Challenge getChallenge(@PathVariable String id) {
		return challengeService.getChallengeById(Integer.valueOf(id));
	}

	@RequestMapping("/challenges/category")
	private List<Challenge> getChallengesByCategory(@PathVariable String category) {
		return challengeService.findByCategory(category);
	}

	@RequestMapping("/challenges/first")
	private List<Challenge> getFirstChallenges() {
		return challengeService.findByCategory("first");
	}

	@RequestMapping(method=RequestMethod.PUT,value="/challenges/nofollow")
	private List<Challenge> getChallengesNotFollow(@RequestBody int[] challsFollow) {
		return challengeService.findChallengesNotFollow(challsFollow);
	}

	@RequestMapping(method=RequestMethod.PUT,value="/challenges/user/{id}")
	public List<ChallengeStatus> getStatusChallenges(@PathVariable String id,@RequestBody ArrayList<Integer> challsFollow) {
		return challengeDoneService.returnTheChallengeStatus(Integer.valueOf(id),challsFollow);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/challenges")
	public void addChall(@RequestParam String challType,@RequestParam String challCateogry,@RequestParam String description,@RequestParam String points) {
		challengeService.addChallenge(challType,challCateogry,description,Integer.valueOf(points));
	}	
	
	@RequestMapping(method=RequestMethod.POST,value="/challenges/do")
	public ChallengeDoneDTO addChallDone(@RequestBody DoChallenge doChallenge) {
		return challengeDoneService.doAChallenge(doChallenge.iduser,doChallenge.idchall);
	}
}