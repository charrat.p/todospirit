package com.cpe.springboot.challenge.model;

public enum ChallengeType {
	DAILY,WEEK,MONTH,YEAR;
}
