package com.cpe.springboot.challenge.controller;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.cpe.springboot.challenge.model.Challenge;
import com.cpe.springboot.challenge.model.ChallengeCategory;
import com.cpe.springboot.challenge.model.ChallengeDone;
import com.cpe.springboot.challenge.model.ChallengeDoneDTO;
import com.cpe.springboot.challenge.model.ChallengeStatus;
import com.cpe.springboot.challenge.model.ChallengeType;
import com.cpe.springboot.spirit.controller.SpiritService;
import com.cpe.springboot.user.controller.UserService;
import com.cpe.springboot.user.model.UserModel;

@Service
public class ChallengeDoneService {
	private final ChallengeDoneRepository challengeDoneRepository;
	private final UserService userService;
	private final ChallengeService challengeService;
	private final SpiritService spiritService;

	public ChallengeDoneService(ChallengeDoneRepository challengeDoneRepository, UserService userService, ChallengeService challengeService, SpiritService spiritService) {
		this.challengeDoneRepository = challengeDoneRepository;
		this.userService = userService;
		this.challengeService = challengeService;
		this.spiritService = spiritService;
	}

	public ChallengeDoneDTO doAChallenge(int idPlayer, int idChallenge) {
		ChallengeDoneDTO ret = null;
		UserModel user = userService.getUser(idPlayer);
		Challenge chall = challengeService.getChallengeById(idChallenge);
		if (user.followTheChallenge(chall.getId())) {
			List<ChallengeDone> listChall = challengeDoneRepository.findByUserAndChall(idPlayer, idChallenge);
			if (listChall.size() > 0) {
				ChallengeDone challDone = listChall.get(0);
				if (!sameTime(chall, challDone.getDate())) {
					challDone.setDate(new Date());
					challengeDoneRepository.save(challDone);
					ret = new ChallengeDoneDTO(userService.addPoints(idPlayer, chall.getPoints()),nextDate(LocalDate.ofInstant(challDone.getDate().toInstant(), ZoneId.systemDefault()),chall),spiritService.spiritWinXp(user.getIdSpirit(), chall.getPoints()));					 
				}
			}
			else { 
				ChallengeDone challDone = new ChallengeDone(idChallenge,idPlayer);
				challengeDoneRepository.save(challDone);
				ret = new ChallengeDoneDTO(userService.addPoints(idPlayer, chall.getPoints()),nextDate(LocalDate.ofInstant(challDone.getDate().toInstant(), ZoneId.systemDefault()),chall),spiritService.spiritWinXp(user.getIdSpirit(), chall.getPoints()));					 
				if (chall.getCategory() == ChallengeCategory.FIRST) {
					userService.removeChallenge(chall.getId(),user.getId());
				}
			}
		}
		return ret;
	}
	
	private boolean sameTime(Challenge chall, Date date) {
		ChallengeType challType = chall.getType();
		Date now = new Date();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
		if (challType == ChallengeType.WEEK) {
			fmt = new SimpleDateFormat("w");	
		}
		else if (challType == ChallengeType.MONTH) {
			fmt = new SimpleDateFormat("yyyyMM");	
		}
		else if (challType == ChallengeType.YEAR) {
			fmt = new SimpleDateFormat("yyyy");
		}
		return fmt.format(date).equals(fmt.format(now)); 
	}
	
	public List<ChallengeStatus> returnTheChallengeStatus(int idPlayer, ArrayList<Integer> challengesFollow) {
		List<ChallengeStatus> ret = new ArrayList<ChallengeStatus>();
		UserModel user = userService.getUser(idPlayer);
		for(int i = 0; i < challengesFollow.size(); i++) {
			Challenge chall = challengeService.getChallengeById(challengesFollow.get(i));
			ChallengeStatus challStatus = new ChallengeStatus(chall);
			List<ChallengeDone> listChall = challengeDoneRepository.findByUserAndChall(idPlayer, chall.getId());	
			if (listChall.size() > 0) {
				ChallengeDone challDone = listChall.get(0);
				if (sameTime(chall, challDone.getDate())) {
					challStatus.status = false; 
					challStatus.nextTime = nextDate(LocalDate.ofInstant(challDone.getDate().toInstant(), ZoneId.systemDefault()),chall);
				}
			}
			ret.add(challStatus);
		}
		return ret;
	}
	
	private LocalDate nextDate(LocalDate ret, Challenge chall) {
		ChallengeType challType = chall.getType();
		if (challType == ChallengeType.DAILY) {
			ret = ret.plusDays(1);	
		}
		else if (challType == ChallengeType.WEEK) {
			ret = ret.plusWeeks(1);	
		}
		else if (challType == ChallengeType.MONTH) {
			ret = ret.plusMonths(1);	
		}
		else if (challType == ChallengeType.YEAR) {
			ret = ret.plusYears(1);	
		}
		return ret;
	}
}
