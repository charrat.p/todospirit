package com.cpe.springboot.challenge.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.cpe.springboot.user.model.UserModel;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Challenge implements Serializable {

		private static final long serialVersionUID = 2733795832476568049L;
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private Integer id;
		private ChallengeType type;
		private ChallengeCategory category;
		private String description;
		private Integer points;
		
		public Challenge() {
			super();
			this.type = ChallengeType.DAILY;
			this.category = ChallengeCategory.WORK;
			this.description = "";
			this.points = 0;
		}
		
		public Challenge(ChallengeType challType,ChallengeCategory challCategory, String description, int points) {
			super();
			this.type = challType;
			this.category = challCategory;
			this.description = description;
			this.points = points;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public ChallengeType getType() {
			return type;
		}

		public void setType(ChallengeType type) {
			this.type = type;
		}

		public ChallengeCategory getCategory() {
			return category;
		}

		public void setCategory(ChallengeCategory category) {
			this.category = category;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public Integer getPoints() {
			return points;
		}

		public void setPoints(Integer points) {
			this.points = points;
		}		
}
