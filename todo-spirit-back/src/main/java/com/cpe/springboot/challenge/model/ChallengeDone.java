package com.cpe.springboot.challenge.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.cpe.springboot.user.model.UserModel;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class ChallengeDone implements Serializable {

		private static final long serialVersionUID = 2733795832476568049L;
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private Integer id;
		private Integer chall;
		private Integer user;
		private Date date;
		
		public ChallengeDone() {
			super();
			this.chall = 0;
			this.user = 0;
			this.date = new Date(); 
		}

		public ChallengeDone(Integer chall, Integer user) {
			super();
			this.chall = chall;
			this.user = user;
			this.date = new Date(); 
		}



		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getChall() {
			return chall;
		}

		public void setChall(Integer chall) {
			this.chall = chall;
		}

		public Integer getUser() {
			return user;
		}

		public void setUser(Integer user) {
			this.user = user;
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}
}