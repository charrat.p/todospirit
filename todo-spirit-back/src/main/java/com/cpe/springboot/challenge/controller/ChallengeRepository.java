package com.cpe.springboot.challenge.controller;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.challenge.model.Challenge;
import com.cpe.springboot.challenge.model.ChallengeCategory;

public interface ChallengeRepository extends CrudRepository<Challenge, Integer> {
	List<Challenge> findByCategory(ChallengeCategory challengeCategory);
}
