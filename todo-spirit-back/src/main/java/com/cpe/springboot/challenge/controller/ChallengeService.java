package com.cpe.springboot.challenge.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.cpe.springboot.challenge.model.Challenge;
import com.cpe.springboot.challenge.model.ChallengeCategory;
import com.cpe.springboot.challenge.model.ChallengeType;

@Service
public class ChallengeService {
	private final ChallengeRepository challengeRepository;

	public ChallengeService(ChallengeRepository challengeRepository) {
		this.challengeRepository = challengeRepository;
	}

	public List<Challenge> getAllChallenges() {
		List<Challenge> challList = new ArrayList<>();
		challengeRepository.findAll().forEach(challList::add);
		return challList;
	}
	
	public Challenge getChallengeById(int id) {
		Challenge ret = null;
		Optional<Challenge> cget = challengeRepository.findById(id);
		if (cget.isPresent()) {
			ret = cget.get();
		}
		return ret;
	}
	
	public List<Challenge> findByCategory(String category) {
		return challengeRepository.findByCategory(convertStringToChallengeCategory(category));
	}
	
	public void addChallenge(String challType, String challCateogry, String description, int points) {
		Challenge challenge = new Challenge(convertStringToChallengeType(challType), convertStringToChallengeCategory(challCateogry), description, points);
		challengeRepository.save(challenge);
	}
	
	private ChallengeType convertStringToChallengeType(String c) {
		ChallengeType ret = null;
		if (c == "daily") {
			ret = ChallengeType.DAILY;
		}
		else if (c == "week") {
			ret = ChallengeType.WEEK;
		}
		else if (c == "month") {
			ret = ChallengeType.MONTH;
		}
		else {
			ret = ChallengeType.YEAR;	
		}
		return ret;
	}
	
	private ChallengeCategory convertStringToChallengeCategory(String c) {
		ChallengeCategory ret = null;
		if (c == "work") {
			ret = ChallengeCategory.WORK;
		}
		else if (c == "health") {
			ret = ChallengeCategory.HEALTH;
		}
		else if (c == "first") {
			ret = ChallengeCategory.FIRST;
		}
		else {
			ret = ChallengeCategory.CULTUR;	
		}
		return ret;
	}

	public List<Challenge> findChallengesNotFollow(int[] challFollows) {
		List<Challenge> ret = new ArrayList<>();
		Iterator<Challenge> challengeCheck = challengeRepository.findAll().iterator();
		while(challengeCheck.hasNext()) {
			 Challenge chall = challengeCheck.next();
			 if(!Arrays.stream(challFollows).anyMatch(n->n==chall.getId()) & !chall.getCategory().equals(ChallengeCategory.FIRST)) {
				 ret.add(chall);
			 }
		}		
		return ret;
	}
}
