package com.cpe.springboot.challenge.model;

import java.time.LocalDate;

public class ChallengeDoneDTO {

		public Integer score;
		public LocalDate nextTime;
		public Integer spiritXP;
		
		public ChallengeDoneDTO(Integer score, LocalDate nextTime, Integer spiritXP) {
			this.score = score;
			this.nextTime = nextTime;
			this.spiritXP = spiritXP;
		}
}