package com.cpe.springboot.challenge.model;

import java.time.LocalDate;

public class ChallengeStatus {
	public Challenge chall;
	public Boolean status;
	public LocalDate  nextTime;
	
	public ChallengeStatus(Challenge chall) {
		this.chall = chall;
		this.status = true;
		this.nextTime = LocalDate.now();		
	}	
}