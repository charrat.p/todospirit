import {useSelector} from 'react-redux';
import axios from 'axios';
import { useEffect, useState } from 'react';
import Challenge from '../base/Challenge';
import '../../style/tuto.css'
import '../../style/challenge.css';
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch } from 'react-redux';
import { updateUser } from '../../actions';
import ModalTuto from '../base/ModalTuto';
import React from 'react'

function ChoiceChallenge() {
    let user = useSelector((state: any) => state.userReducer.current_user);
    const dispatch=useDispatch();
    const [display, setdisplay] = useState(<p></p>);
    const [displayResult, setdisplayResult] = useState(<p></p>);
    const dialog = "Pour renforcer ton esprit, tu doit te confronter à des challenges."
    const [seeNextStep, setNextStep] = useState(false);
        
    function findNewChallenges() {
        axios.get('http://127.0.0.1:8080/challenges/first',  {})
        .then(res => { 
            setdisplay(res.data.map((challenge : any) => <React.Fragment key={"challenge-"+challenge.id}><div id={"challenge-"+challenge.id} className={`col-4 offset-1`}><Challenge challenge={challenge} follow={false} doAnAction={chooseChallenge}  status={true}></Challenge></div><div className='col-1'></div></React.Fragment>)); 
        }); 
    }
    
    useEffect(() => {
        if (user.idchallengesFollow.length === 0) {
            findNewChallenges()
        }
    },[]);

    function chooseChallenge(challengeChoose: number) {
        let order = challengeChoose;
        axios.put('http://127.0.0.1:8080/user/'+user.id+'/challenges/',  JSON.stringify(order) , { headers: {  "Content-Type": "application/json"  }})
        .then(res => {
            if(res.data !== false) {
                user.idchallengesFollow.push(challengeChoose);
                setdisplayResult(<span style={{color: "green"}}>Vous suivez un nouveau challenge</span>);
                dispatch(updateUser(user));
                setNextStep(true);
            }
            else {
                setdisplayResult(<span style={{color: "red"}}>Erreur, réessayer</span>);
            }
        });
    }

    return (
        <div className="parent">
            <div className="div1"> 
                <div className="bg-radient vh-100">
                    <div className="container h-100">
                        <div className="row d-flex justify-content-center align-items-center h-100">
                            <div className="col-12 h-75">
                                <div className="card bg-dark text-white h-100">
                                    <div className="card-body text-center">
                                        <div className="pb-2 h-100">
                                            <div className='col-12 h-10 mt-2'>
                                                <h2>Sélectionne une épreuve</h2>
                                            </div>
                                            <div className='col-12 row h-70 mt-3'>
                                                {display}
                                            </div>
                                            <div className='col-12'>
                                                {displayResult}
                                            </div>
                                            <div className='col-12'>
                                                { seeNextStep && <NavLink to="/" ><p id="soldPlayer" className='float-right'>Prochaine étape</p></NavLink> }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="div2">
                <ModalTuto dialog={dialog}></ModalTuto>
            </div>
        </div>
    );
  }
  export default ChoiceChallenge;