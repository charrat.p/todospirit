import { useDispatch } from 'react-redux';
import axios from 'axios';
import { useState } from 'react';
import { updateUser } from '../../actions';
import '../../style/login.css'

function Connection() {
    const dispatch=useDispatch();
    
    const [login,setLogin]=useState('');
    const [pwd,setPwd]=useState('');
    const [result,setResult]=useState(<></>);

    function handleChange (event: any){
      setLogin(event.target.value );
    }
    
    function handleChangePWD( event: any ){
      setPwd(event.target.value );
    }

    function connect(event: any) {
        event.preventDefault();
        if (pwd != '' && login != '' ) {
            const order = {login: login, pwd: pwd };
            axios.put('http://127.0.0.1:8080/connexion', JSON.stringify(order) , { headers: {  "Content-Type": "application/json"  }})
            .then(res => { 
                if (res.data != "") {
                    dispatch(updateUser(res.data));
                    setResult(<span style={{color: "green"}}>OK</span>);
                }
                else {
                    setResult(<span style={{color: "red"}}>Erreur, login ou mot de passe faux</span>);
                }
            })
        } 
    }
    
    return (
        <div className="bg-radient vh-100">
            <div className="container py-5 h-100">
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col-12 col-md-8 col-lg-6 col-xl-5">
                        <div className="card bg-dark text-white">
                            <div className="card-body text-center">
                                <div className="mb-md-5 mt-md-4">
                                    <h2 className="fw-bold mb-2 text-uppercase">TODO-SPIRIT</h2>
                                    <form onSubmit={connect}>
                                        <div className="form-outline form-white mb-4">
                                            <label className="form-label" htmlFor='login'>Login</label>
                                            <input id="login" value={login} onChange={handleChange}  className="form-control form-control-lg"></input>
                                        </div>
                                        <div className="form-outline form-white mb-4">
                                            <label className="form-label" htmlFor='pwd'>Password</label>
                                            <input id="pwd" type="password" value={pwd} onChange={handleChangePWD} className="form-control form-control-lg"></input>
                                        </div>
                                        <input type="submit" id="submit" className='btn btn-outline-light btn-lg px-5'></input>
                                        <div>
                                            <p className="my-1"><a href="#!" className="text-white-50 fw-bold">Créer son compte</a></p>
                                        </div>
                                        {result}
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Connection;
