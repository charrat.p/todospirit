import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import {useSelector} from 'react-redux';
import axios from 'axios';
import Spirit from '../base/Spirit';
import { updateUser } from '../../actions';
import '../../style/spirit.css'
import '../../style/tuto.css'
import { NavLink, useNavigate } from "react-router-dom";
import ModalTuto from '../base/ModalTuto';

function ChoiceSpirit() {
    let user = useSelector((state: any) => state.userReducer.current_user);
    const dispatch=useDispatch();
    const navigate = useNavigate();
    const [display, setdisplay] = useState(<p></p>);
    const [displayResult, setdisplayResult] = useState(<p></p>);
    const [seeNextStep, setNextStep] = useState(false);
    const dialog = 'Pour devenir un exorsite, il faut que tu choisise ton esprit compagnon ! C\'est avec lui que tu évoluras.';
    
    function findNewSpirits() {
        axios.get('http://127.0.0.1:8080/spirits/detection',  {})
        .then(res => { 
            setdisplay(res.data.map((spirit : any) => <div key={"spirit-"+spirit.id} id={"spirit-"+spirit.id} className="col-4"><div><Spirit spirit={spirit} mode="tuto"></Spirit></div><div><input type="submit" value={"Le choisir"}  onClick={() => {selectSpirit(spirit.id)}}></input></div></div>)); 
        }); 
    }
    
    useEffect(() => {
        if (user.idSpirit === -1) {
            findNewSpirits()
        }
    },[]);

    function selectSpirit(id: number) {
        let order = user.id;
        axios.post('http://127.0.0.1:8080/spirits/'+id,  JSON.stringify(order) , { headers: {  "Content-Type": "application/json"  }})
        .then(res => {  
            if(res.data !== -1) {
                user.idSpirit = res.data;
                setdisplayResult(<span style={{color: "green"}}>L'esprit est passé sous votre controle</span>);
                dispatch(updateUser(user)); 
                setNextStep(true);          
            }
            else {
                setdisplayResult(<span style={{color: "red"}}>Erreur, réessayer</span>);
            }
        });
    }

    return (
        <div className="parent">
            <div className="div1"> 
                <div className="bg-radient vh-100">
                    <div className="container h-100">
                        <div className="row d-flex justify-content-center align-items-center h-100">
                            <div className="col-12 h-75">
                                <div className="card bg-dark text-white h-100">
                                    <div className="card-body text-center">
                                        <div className="pb-2 h-100">
                                            <div className='col-12 h-10 mt-2'>
                                                <h2>Choisi ton esprit </h2>
                                            </div>
                                            <div className='col-12 row h-70'>
                                                {display}
                                            </div>
                                            <div className='col-12 h-10 mt-2'>
                                            </div>
                                            <div className='col-12'>
                                                {displayResult}
                                            </div>
                                            <div className='col-12'>
                                                { seeNextStep && <NavLink to="/" ><p id="soldPlayer" className='float-right'>Prochaine étape</p></NavLink> }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="div2">
                <ModalTuto dialog={dialog}></ModalTuto>
            </div>
        </div>
    );
  }
  export default ChoiceSpirit;