function Spirit(props: any) {
    let spirit = props.spirit;
    let mode = props.mode;
    return (
        <>
            {
            mode === "tuto" && 
                <>
                <div className="spirit-image col-12"><img src={spirit["urlImg"]} alt="esprit"  width="30%" /></div>
                <div className="spirit-name  col-12">{spirit.name}</div>
                </>
            }
            {
            mode === "main" && 
                <div className="spirit-image"><img src={spirit["urlImg"]} alt="esprit"  width="15%" /></div>
            }

        </>
    );
}
    
export default Spirit;