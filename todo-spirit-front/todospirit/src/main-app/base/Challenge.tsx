import '../../style/challenge.css';

function Challenge(props: any) {
    let challenge = props.challenge;
    const follow = props.follow;
    const status = props.status;
    let nextTime;
    let title;
    if (props.nextTime) {
        nextTime = props.nextTime;
    }
    if (follow) {
        title = 'Le valider';
    }
    else {
        title = 'Le suivre';
    }
    
    return (
        <div className={"card h-100 max-200 challengeType"+challenge.category}>
            <div className="card-body background-black h-100 d-flex flex-column">
                <h5 className="card-title">{challenge["type"]} | {challenge["points"]} points</h5>
                <p className="card-text">{challenge["description"]}</p>
                { (status) && <input type="submit" value={title} className={"mt-auto mx-3"} onClick={() => props.doAnAction(challenge.id)} />}
                { (!status && nextTime !== undefined) && <p className={"mt-auto"}>Prochaine fois le : {nextTime} </p>}
                </div>
        </div>
    );
}
export default Challenge;