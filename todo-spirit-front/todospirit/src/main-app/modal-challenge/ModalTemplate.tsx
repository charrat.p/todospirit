import { useEffect, useState } from 'react';
import Challenge from '../base/Challenge';
import Filter from './Filter';

function ModalTemplate(props: any) {
  const [challenges, setChallenges] = useState([]);
  const [displayChallenges, setdisplay] = useState(<p></p>);
  const [category, setCategory] = useState("");
  const [type, setType] = useState("");
  const modalTitle = props.modalTitle;
  const follow = props.follow;
  let modalResult = props.modalResult;

  useEffect(()=>{
    setChallenges(props.challenges);
  },[props.challenges]);

  useEffect(() => {
    sortChallengesByFilters();
  },[category,type,challenges]);
  
  function sortChallenge(e : any)  {
    let description = e.target.value;
    let newDisplay : any;
    if (description.length > 3) {   
      let sortChallenges: Array<{}> = challenges.filter((challengeTest: any) => challengeTest.chall["description"].includes(description));
      newDisplay = sortChallenges.map((challenge : any) => <div key={"challenge-"+challenge.chall.id} id={"challenge-"+challenge.chall.id} className={`col-4 mt-2`} ><Challenge challenge={challenge.chall} follow={follow} doAnAction={doAction} status={challenge.status} nextTime={challenge.nextTime}></Challenge></div>)
    }
    else if (description.length === 0) {
      newDisplay = challenges.map((challenge : any) => <div key={"challenge-"+challenge.chall.id} id={"challenge-"+challenge.chall.id} className={`col-4 mt-2`} ><Challenge challenge={challenge.chall} follow={follow} doAnAction={doAction} status={challenge.status} nextTime={challenge.nextTime}></Challenge></div>)
    }

    if ( newDisplay !== undefined ) {
      setdisplay(newDisplay);
    }
}

function selectType(e : any)  {
    setType(e.target.value);
}

function selectCategory(e : any)  {
    setCategory(e.target.value);
}

function sortChallengesByFilters() {
    let sortChallenges: Array<{}>;
    if ( type !== "" && category === "" ) {   
        sortChallenges = challenges.filter((challengeTest: any) => challengeTest.chall.type === type ); 
    }
    else if ( type === "" && category !== "" ) {   
        sortChallenges = challenges.filter((challengeTest: any) => challengeTest.chall.category === category ); 
    }
    else if ( type !== "" && category !== "" ) {   
        sortChallenges = challenges.filter((challengeTest: any) => challengeTest.chall.category === category && challengeTest.chall.type === type); 
    }
    else {
        sortChallenges = challenges;
    }
    let newDisplay: any = sortChallenges.map((challenge : any) => <div key={"challenge-"+challenge.chall.id} id={"challenge-"+challenge.chall.id} className={`col-4 mt-2`} ><Challenge challenge={challenge.chall} follow={follow} doAnAction={doAction}  status={challenge.status} nextTime={challenge.nextTime}></Challenge></div>)
    setdisplay(newDisplay);
  }


  function doAction(challengeChoose: number) {
    props.doAnAction(challengeChoose);
  }

  return (
    <div className="view-overlay ">
        <div className="view-wrapper">
        <div className="view bg-view bg-color">
          <div className="view-header">
            <h4>{modalTitle}</h4>
            <button type="button" className="close" onClick={()=>{props.closeModal(false)}}>
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="view-body p2">
              <Filter selectType={selectType} selectCategory={selectCategory} sortChallenge={sortChallenge}></Filter>
              <div className='row scroller'>
                {displayChallenges}
              </div>
              <div className="w-100">
                {modalResult}
              </div>
          </div>
        </div>
      </div>
    </div>
  ); 
}

export default ModalTemplate;