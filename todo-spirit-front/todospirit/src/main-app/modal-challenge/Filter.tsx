function Filter(props: any) {
  return (
    <div className='w-100 mb-2 d-flex flex-row'>
      <input type="text" onChange={props.sortChallenge}></input>
      <select onChange={props.selectType}>
        <option value="">---</option>
        <option value="DAILY">Jours</option>
        <option value="WEEK">Semaine</option>
        <option value="MONTH">Mois</option>
        <option value="YEAR">Année</option>
      </select>
      <select onChange={props.selectCategory}>
        <option value="">---</option>
        <option value="WORK">Travail</option>
        <option value="HEALTH">Santé</option>
        <option value="CULTUR">Culture</option>
      </select>
    </div>
  );
}

export default Filter;