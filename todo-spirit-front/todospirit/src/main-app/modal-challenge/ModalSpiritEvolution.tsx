import { useEffect, useState } from "react";
import '../../style/modalEvolution.css';

function ModalSpiritEvolution(props: any) {
    const [viewEvolution, setViewEvolution] = useState(false);
    const [oldSpirit, setOldSpirit] = useState({urlImg:"",name:""});
    const [newSpirit, setNewSpirit] = useState({urlImg:"",name:""});
    const [seeOldSpirit, setSeeOld] = useState(false);
    const [seeNewSpirit, setSeeNew] = useState(false);
    const [interval, setIntervalEvolution] = useState<any|undefined>();
    const [spirits, setSpirits] = useState(false);
    const [index, increment] = useState(0);
    const [beforeAnimation, setBefore] = useState(false);
    const [afterAnimation, setAfter] = useState(false);
    
    useEffect(()=>{ 
        setViewEvolution(props.viewEvolution);
        beginEvolution(); 
    },[props.viewEvolution]);
    
    useEffect(()=>{ 
        if (props.oldSpirit !== undefined) {
            setOldSpirit(props.oldSpirit);
        }
    },[props.oldSpirit]);
    
    useEffect(()=>{ 
        if (props.newSpirit !== undefined){
            setNewSpirit(props.newSpirit);
        }
    },[props.newSpirit]);

    useEffect(() => {
        beginEvolution();
    },[oldSpirit,newSpirit]);

    useEffect(() => {
        if (spirits) {
            const intervalEvolution = setInterval(() => {
                increment((prevTime) => prevTime + 1)
            }, 500);
            setIntervalEvolution(intervalEvolution);
        }
    }, [spirits]);

    useEffect(()=>{
        makeAnimation();
    },[index])

    function beginEvolution() {
        if ( oldSpirit.urlImg !== "" && newSpirit.urlImg !== "" ) {
            setSpirits(true);
        }
    }

    function makeAnimation() {
        if (index === 0 || index === 1 ) {
         setBefore(true);   
        }
        else if (index === 2 ) {
            setBefore(false);
            setSeeOld(true);
        }
        else if (index === 3 ) {
            setSeeNew(true);
            setSeeOld(false);
        }
        else if (index === 4) {
            setSeeNew(false);
            setSeeOld(true);
        }
        else if (index === 5) {
            setSeeNew(true);
            setSeeOld(false);
        }
        else if (index === 6) {
            setSeeNew(false);
            setSeeOld(true);
        }
        else if (index === 7) {
            setSeeNew(true);
            setSeeOld(false);
        }
        else if (index === 8) {
            setSeeNew(false);
            setSeeOld(true);
        }
        else if (index === 9) {
            setSeeNew(true);
            setSeeOld(false);
        }
        else {
            clearInterval(interval);
            setSeeNew(false);
            setSeeOld(false);
            setAfter(true);
        }    
    }

    return (
        <>
        { (viewEvolution && spirits) &&
            <div className="vh-100 grid-table">
                <div className="vh-100 cell1 bg-opacity" defaultValue=""></div>
                <div className="cell2 Vh-100">
                    <div id="evolution" className="marge-auto table-evolution">
                        <button type="button" className="btnclose close" onClick={()=>{props.closeModal(false)}}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {beforeAnimation && <div className="spirit-image divbas"><img src={oldSpirit.urlImg} alt="esprit"  width="40%" className="max-height-img-evolution"/></div>}
                        {beforeAnimation && <p className="labelevolution">Votre {oldSpirit.name.toUpperCase()} se sent bizarre ...</p>}
            
                        {seeOldSpirit && <div className="spirit-image divbas"><img src={oldSpirit.urlImg} alt="esprit"  width="40%" className="max-height-img-evolution filter-white"/></div>}
                        {seeNewSpirit && <div className="spirit-image divhaut"><img src={newSpirit.urlImg} alt="esprit"  width="40%" className="max-height-img-evolution filter-white"/></div>}
                        {(seeNewSpirit || seeOldSpirit) && <p className="labelevolution"> Oh, il évolue ! </p>}
                    
                        {afterAnimation && <div className="spirit-image divhaut"><img src={newSpirit.urlImg} alt="esprit"  width="40%" className="max-height-img-evolution"/></div>}
                        {afterAnimation && <p className="labelevolution">Votre {oldSpirit.name.toUpperCase()} est devenu un {newSpirit.name.toUpperCase()}</p>}
                    </div>
                </div>
            </div>
        }
        </>
    );
}

export default ModalSpiritEvolution;