import axios from 'axios';
import { useEffect, useState } from 'react';
import {useDispatch, useSelector} from 'react-redux';
import { updateSpirit, updateUser } from '../../actions';
import '../../style/modal.css'
import '../../style/challenge.css'
import ModalTemplate from './ModalTemplate';

function ModalDoAChallenge(props: any) {
  const [displayResult, setResult] = useState(<p></p>);
  const [challenges, setChallengesStatus] = useState([{},{}]);
  const [displayChallenges, setdisplayChall] = useState(false);
  const dispatch = useDispatch();
  let view = props.viewModal;
  let user = props.user;
  let spirit = props.spirit;
  let modalTitle = "Valider un challenge";
  
  useEffect(() => {
    if (view) {
      recoverStatus();
      setResult(<></>);  
    }
  },[view]);

  useEffect(() => {
    if (view) {
      setdisplayChall(true);
    }
  },[challenges]);

  function recoverStatus() {
      let order = user.idchallengesFollow;
      axios.put('http://127.0.0.1:8080/challenges/user/'+user.id,  JSON.stringify(order) , { headers: {  "Content-Type": "application/json"  }})
      .then(res => { 
        setChallengesStatus(res.data);
      }); 
  }

  function doAChallenge(challengeChoose: number) {
    let order = {"idchall":challengeChoose,"iduser":user.id};
    axios.post('http://127.0.0.1:8080/challenges/do',  order , { headers: {  "Content-Type": "application/json"  }})
    .then(res => { 
      if ( res.data === "" ) {
        setResult(<span style={{color:'red'}}>Erreur, challenge déjà réalisé.</span>);
      }
      else {
        user.points = res.data.score;
        dispatch(updateUser(user)); 
        setResult(<span style={{color:'green'}}>Challenge validé</span>);
        let challengesUpdated: any = challenges;
        challengesUpdated.filter((challenge:any) => challenge.chall.id === challengeChoose)[0].status = false;
        challengesUpdated.filter((challenge:any) => challenge.chall.id === challengeChoose)[0].nextTime = res.data.nextTime;
        setChallengesStatus([...challengesUpdated]);
        if (res.data.spiritXP !== 0) {
          spirit.xp = res.data.spiritXP;
          dispatch(updateSpirit(spirit));
          let evoluate = spirit.xp >= spirit.xpToNextLevel 
          props.setModalAlert(evoluate);
        }
      }     
    });
  }

  function closeModal(ret: any) {
    props.changeDoModal(ret);
    setdisplayChall(false);
    setResult(<></>);
  }

  return (
    <> { (view && displayChallenges)  && <ModalTemplate key={"test"} challenges={challenges} modalTitle={modalTitle} modalResult={displayResult} closeModal={closeModal} doAnAction={doAChallenge} follow={true}></ModalTemplate> } </>
  );
}

export default ModalDoAChallenge;