import { useEffect, useState } from "react";
import '../../style/modalTuto.css';

function ModalAlertSpirit(props: any) {
    
    const dialog = "Votre esprit va pouvoir évoluer !";
    const dialogArray = dialog.split(' ');
    const [index, increment] = useState(1);
    const [dialogDisplay, setDialog] = useState(dialogArray[0]);
    const [viewModalAlert, setViewAlert] = useState(false);

    function hideModal() {
        props.setModalAlert(false);
    }

    useEffect(() => {
      const myInterval = setInterval(() => {
        increment((prevTime) => prevTime + 1);
      }, 300);
      return () => clearInterval(myInterval);
    }, [viewModalAlert]);

    useEffect(()=> {
        if(dialogArray.length > index) {
            setDialog((prevDialog: any) => prevDialog + ' '+dialogArray[index]);   
        }
    }, [index]);

    useEffect(()=> {
        setViewAlert(props.viewModal)
    }, [props.viewModal]);

    return (
        <>
        { (viewModalAlert) &&
            <div className="vh-100 grid-table">
                <div className="vh-100 cell1 bg-opacity" defaultValue=""></div>
                <div className="cell2 Vh-100">
                    <div id="dialog" className="margauto row">
                        <div className="col-12">{dialogDisplay}</div>
                        <div className="col-12 text-right"><a className="linkModal" onClick={()=>{hideModal()}}>Cliquer pour fermer</a></div>
                    </div>
                </div>
            </div>
        }
        </>
    );
}
    
export default ModalAlertSpirit;