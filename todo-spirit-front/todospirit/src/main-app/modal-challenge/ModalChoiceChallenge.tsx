import {useSelector} from 'react-redux';
import axios from 'axios';
import { useEffect, useState } from 'react';
import Challenge from '../base/Challenge';
import '../../style/challenge.css';
import '../../style/modalTemplate.css'
import { useDispatch } from 'react-redux';
import { updateUser } from '../../actions';
import ModalTemplate from './ModalTemplate';

function ModalChoiceChallenge(props: any) {
    const modalTitle = "Suivre un nouveau challenge";
    const [displayAllChallenges, setAllChall] = useState(false);
    const dispatch=useDispatch();
    const [challenges, setChallenges] = useState([]);
    const [displayResult, setdisplayResult] = useState(<p></p>);
    let view = props.viewModal;
    let user = props.user;
      
    useEffect(() => {
        findNewChallenges();
    },[]);
    
    
    function findNewChallenges() {
        if ( challenges.length === 0 ){
            axios.put('http://127.0.0.1:8080/challenges/nofollow',  user.idchallengesFollow ,  {})
            .then(res => { 
                let newChallenges: any = [];
                res.data.forEach((challenge: any) => newChallenges.push({chall:challenge, status:true}));
                setChallenges(newChallenges); 
                setAllChall(true);
            }); 
        }
    }
    
    function chooseChallenge(challengeChoose: number) {
        let order = challengeChoose;
        axios.put('http://127.0.0.1:8080/user/'+user.id+'/challenges/',  JSON.stringify(order) , { headers: {  "Content-Type": "application/json"  }})
        .then(res => {
            if(res.data !== false) {
                user.idchallengesFollow.push(challengeChoose);
                setdisplayResult(<span style={{color: "green"}}>Vous suivez un nouveau challenge</span>);
                dispatch(updateUser(user));
                let challengesNotFollow = challenges.filter((challenge: any) => challenge.id !== challengeChoose);
                setChallenges(challengesNotFollow);
            }
            else {
                setdisplayResult(<span style={{color: "red"}}>Erreur, réessayer</span>);
            }
        });
    }
    
    function closeModal(ret: any) {
        props.changeListModal(ret);
        setdisplayResult(<></>);
    }

    return (
        <> {(view && displayAllChallenges)  && <ModalTemplate challenges={challenges} modalTitle={modalTitle} modalResult={displayResult} closeModal={closeModal} doAnAction={chooseChallenge} follow={false}></ModalTemplate> } </>
    );
}

export default ModalChoiceChallenge;