import { useEffect, useState, useRef } from "react";
import '../../style/modalEncouragement.css';

function ModalEncouragement(props: any) {
    const [viewEncouragement, setViewEncouragement] = useState(false);
    const [posX, setPosX] = useState(0);
    const [posY, setPosY] = useState(0);
    const [limit, setLimit] = useState(0);
    const intervalRef = useRef<NodeJS.Timer>();

    const incrementCounter = () => {
      intervalRef.current = setInterval(() => {
        setPosY(seconds => seconds + 10);
      }, 100);
    };
    
    const resetCounter = () => {
      clearInterval(intervalRef.current);
      intervalRef.current = undefined;
    };


    useEffect(() => {
        if (viewEncouragement) {
                let newLimit = defineNewEncouragement();
                setLimit(newLimit);
        }
    },[viewEncouragement]);

    useEffect(() => {
        if (props.resetEncouragement) {
            resetCounter();
            defineNewEncouragement();
            incrementCounter();
            props.setResetEncouragement(false);
        }
    },[props.resetEncouragement]);

    useEffect(()=>{
        setViewEncouragement(props.viewEncouragement);
    },[props.viewEncouragement])

    useEffect(()=>{
        if (viewEncouragement && limit !== 0) {
            incrementCounter();
        }
    },[limit]);

    useEffect(()=>{
        const d = document.getElementById("encouragement");
        if (d !== null && posX > 0 && posY > 0) {
            d.style.position = "absolute";
            d.style.left = posX+'px';
            d.style.top = posY+'px';
            d.style.opacity = 0.5+(limit-posY)/limit+"";
        }
        if (limit !== 0 && posY >= limit) {
            resetCounter();
            props.setViewEncouragement(false);
            setLimit(0);
        }
    },[posX,posY]);

    function defineNewEncouragement(): number {
        let ret = 0;
        let elem = document.getElementById("areaEncouragement");
        if (elem != undefined) {
            let rect = elem.getBoundingClientRect();
            let newPosX = rect.width/2 + (rect.width/3 * (Math.random()-0.5));
            setPosX(newPosX);
            setPosY(rect.y-rect.height/2);
            ret = rect.y+rect.height/2;
        }
        return ret;
    }

    return (
        <>
        { viewEncouragement &&
            <div id="encouragement">
                <p>{props.encouragement}</p>
            </div>
        }
        </>
    );
}

export default ModalEncouragement;