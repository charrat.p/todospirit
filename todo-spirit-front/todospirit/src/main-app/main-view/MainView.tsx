import { useSelector } from 'react-redux';
import TutoPart from '../tuto-part/TutoPart';
import MainPart from '../main-part/MainPart';

function MainView() {
  let screenDisplay;
  let user = useSelector((state: any) => state.userReducer.current_user);
  
  if(user.newest) {
    screenDisplay = <TutoPart></TutoPart>
  }
  else {
    screenDisplay = <MainPart></MainPart>
  }

  return (
    <div>
      {screenDisplay}
    </div>
  );

}

export default MainView;