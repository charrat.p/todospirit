import { useEffect, useState } from 'react';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import { updateSpirit } from '../../actions';

import ModalDoAChallenge from '../modal-challenge/ModalDoAChallenge';
import ModalChoiceChallenge from '../modal-challenge/ModalChoiceChallenge';
import ModalAlertSpirit from '../modal-challenge/ModalAlertSpirit';
import ModalSpiritEvolution from '../modal-challenge/ModalSpiritEvolution';
import Spirit from '../base/Spirit';

import { faCircleCheck, faCirclePlus, faPenToSquare, faStar, faAnkh  } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../../style/main.css";
import ModalEncouragement from '../modal-challenge/ModalEncouragement';

function MainPart() {
  let user = useSelector((state: any) => state.userReducer.current_user);
  const dispatch = useDispatch();
  const [viewModalDo,setModalDo] = useState(false); 
  const [viewModalList,setModalList] = useState(false);
  const [viewModalAlert,setModalAlert] = useState(false); 
  const [spirit, setSpirit] = useState(useSelector((state: any) => state.userReducer.current_spirit));
  const [newSpirit, setNewSpirit] = useState();
  const [nickname, setNickname] = useState("");
  const [viewInputNickname, changeView] = useState(false);
  const [viewEvolution, setEvolution] = useState(false);
  const [viewEncouragement, setViewEncouragement] = useState(false);
  const [canEvolSpirit,setEvolEsprit] = useState(false);
  const [textEncouragement, setEncouragement] = useState("");
  const [resetEncouragement, setResetEncouragement] = useState(false);
  const encouragements = ["Bonne chance","Vous allez y arriver !","Ne lâchez pas","Coucou","Mon maitre !"];
  
  useEffect(()=>{
    if (spirit === undefined) {
      getSpirit();
    }
  },[]);
    
  function changeDoModal(ret: boolean) {
    setModalDo(ret);
  }

  function changeListModal(ret: boolean) {
    setModalList(ret);
  }

  function getSpirit() {
      axios.get('http://127.0.0.1:8080/spirits/'+user.idSpirit)
      .then(res => { 
        dispatch(updateSpirit(res.data)); 
        setSpirit(res.data);
        if(res.data["xp"] > res.data["xpToNextLevel"]) {
          setEvolEsprit(true);
        }
        setNickname(res.data["nickname"]);
      });  
  }

  function changeNickname() {
    if (nickname !== "" && nickname !== spirit.nickname && viewInputNickname) {
      axios.put('http://127.0.0.1:8080/spirits/'+user.idSpirit+'?nickname='+nickname , { headers: {  "Content-Type": "application/json"  }})
      .then(res => {
        if (res.data) {
          spirit["nickname"] = nickname; 
          dispatch(updateSpirit(spirit));
          changeView(false);
        } 
      });  
    }
    else {
      changeView(!viewInputNickname);
    }
  }

  function modifyNickname(e: any) {
    e.preventDefault();
    setNickname(e.target.value);
  }

  function spiritEvolution() {
    console.log(spirit.id);
    axios.post('http://127.0.0.1:8080/spirits/'+spirit.id+'/evolution' , JSON.stringify(user.id) ,{ headers: {  "Content-Type": "application/json"  }})
      .then(res => {
        if (res.data) {
          setNewSpirit(res.data);
        } 
      });
  }

  function creatAnAlert(ret: boolean) {
    if (ret) {
      setEvolEsprit(true);
    }
    setModalAlert(ret);
  }

  function finishEvolution() {
    setEvolution(false);
    dispatch(updateSpirit(newSpirit));
    setSpirit(newSpirit);
  }

  function wantEncouragement() {
    setEncouragement(encouragements[Math.floor(Math.random() * encouragements.length)]);
    if (!viewEncouragement) {
      setViewEncouragement(true);
    }
    else {
      setResetEncouragement(true);
    }
    
  }

  return (
      <div className="parent"> 
        <div className="div1"> 
          <div className="bg-radient vh-100">
              <div className="container h-100">
                  <div className="row d-flex justify-content-center align-items-center h-100">
                      <div className="col-12 h-75">
                          <div className="card bg-dark text-white h-100">
                              <div className="card-body text-center">
                                  <div className="pb-2 h-100">                                
                                    <>
                                      <div className='d-flex justify-content-between'>
                                        <div className='' onClick={() => changeDoModal(true)}><FontAwesomeIcon icon={faCircleCheck} size="3x" className='icon-hover'/></div>
                                        <div className='' onClick={() => changeListModal(true)}><FontAwesomeIcon icon={faCirclePlus} size="3x" className='icon-hover'/></div>
                                      </div>
                                      {spirit !== undefined && 
                                        <div className='mt-2 px-2 d-flex justify-content-between'>
                                          <div className='d-inline'>
                                            { !viewInputNickname && <p className='text-left d-inline'>{spirit["nickname"] !== "" ? spirit["nickname"].toUpperCase() : spirit["name"].toUpperCase() }</p>}
                                            { viewInputNickname && <input type="text" value={nickname} onChange={modifyNickname} ></input> }
                                            <div onClick={changeNickname} className="d-inline ml-2"><FontAwesomeIcon icon={faPenToSquare} size="xl"/></div>
                                          </div>
                                          <div>
                                            { canEvolSpirit && <div data-toggle="tooltip" data-placement="bottom" title="Time to evoluate !" onClick={() => spiritEvolution()}><FontAwesomeIcon icon={faAnkh} size="2x" className='icon-hover'/></div>}
                                          </div>
                                          <div data-toggle="tooltip" data-placement="bottom" title="Level of">
                                            <div className='d-inline w-100'><h4 className='d-inline'>{spirit["level"]}</h4></div>
                                            <div className='d-inline w-100'><FontAwesomeIcon icon={faStar}/></div>
                                          </div>
                                        </div> }    
                                        {spirit !== undefined && 
                                          <div className='w-100 grid-encouragement' onClick={wantEncouragement}>
                                            <div id="areaEncouragement" className='w-100 text-encouragement'><ModalEncouragement viewEncouragement={viewEncouragement} encouragement={textEncouragement} setViewEncouragement={setViewEncouragement} resetEncouragement={resetEncouragement} setResetEncouragement={setResetEncouragement}></ModalEncouragement></div>
                                            <div className='w-100 spirit-encouragement'><Spirit spirit={spirit} mode="main"></Spirit></div>
                                          </div> 
                                        }
                                        {spirit !== undefined && <div className='w-100' data-toggle="tooltip" data-placement="bottom" title={"Votre esprit "+spirit.nickname+" a "+spirit.xp+"/"+spirit.xpToNextLevel+" xp"}><progress className="monsterXP w-50 m-auto" max={spirit["xpToNextLevel"]} value={spirit["xp"]}></progress></div> }
                                      <>
                                        <ModalDoAChallenge viewModal={viewModalDo} user={user} spirit={spirit} changeDoModal={changeDoModal} setModalAlert={creatAnAlert}></ModalDoAChallenge>
                                        <ModalChoiceChallenge viewModal={viewModalList} user={user} changeListModal={changeListModal}></ModalChoiceChallenge>
                                      </>
                                    </>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      {viewModalAlert && 
        <div className="div2 absolute-top">                            
          <ModalAlertSpirit viewModal={viewModalAlert} setModalAlert={setModalAlert}></ModalAlertSpirit>
        </div>
      } 
      {viewEvolution && 
        <div className="div2 absolute-top">                            
          <ModalSpiritEvolution viewEvolution={viewEvolution} setEvolution={setEvolution} oldSpirit={spirit} newSpirit={newSpirit} closeModal={finishEvolution}></ModalSpiritEvolution>    
        </div>
      } 
    </div>
  );
  }
  export default MainPart;