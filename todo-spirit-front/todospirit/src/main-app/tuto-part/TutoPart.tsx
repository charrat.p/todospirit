import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import { useNavigate } from 'react-router-dom';
import DoAChallenge from '../do-challenge/DoAChallenge';
import ChoiceChallenge from '../find-challenge/ChoiceChallenge';
import ChoiceSpirit from '../find-spirit/ChoiceSpirit';
import { updateUser } from '../../actions';

function TutoPart() {
    const navigate = useNavigate();
    const dispatch=useDispatch();
    let user = useSelector((state: any) => state.userReducer.current_user);    
    let screenDisplay;
    
    if (user.idSpirit === -1) {
        screenDisplay = <ChoiceSpirit></ChoiceSpirit>
    }
    else if (user.idchallengesFollow.length === 0) {
      screenDisplay = <ChoiceChallenge></ChoiceChallenge>
    }
    else if (user.points === 0) {
      screenDisplay = <DoAChallenge></DoAChallenge>
    }
    else {
      axios.put('http://127.0.0.1:8080/user/'+user.id+'/oldest',  {})
      .then(res => {      
        user.newest = false;
        dispatch(updateUser(user));
        navigate("/");           
      });
    }
    return (
      <div>
        {screenDisplay}
      </div>
    );
  }
  export default TutoPart;