import { BrowserRouter as Router, Route ,Link, Routes, Navigate } from "react-router-dom";
import {useSelector} from 'react-redux';
import Connection from "../connected-part/Connection";
import MainView from "../main-view/MainView";

function MainRouter() {
    let user = useSelector((state: any) => state.userReducer.current_user);
    let connected = false;  
    
    if(user.id) 
    {
        connected = true;
    }
    return (
        <Router>
            <Routes>
                { !connected && 
                    <Route path="/connexion" element={<Connection/>} />
                }
                { connected && 
                    <Route path="/accueil" element={<MainView/>} />
                }
                { connected && 
                    <Route path="*" element={<Navigate to="/accueil" />}/>
                }
                { !connected && 
                    <Route path="*" element={<Navigate to="/connexion" />}/>
                }
            </Routes>
        </Router>
      );
    }
    
    export default MainRouter;