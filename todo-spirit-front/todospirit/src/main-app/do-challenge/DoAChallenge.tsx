import {useSelector} from 'react-redux';
import axios from 'axios';
import { useEffect, useState } from 'react';
import Challenge from '../base/Challenge';
import { useDispatch } from 'react-redux';
import { updateUser } from '../../actions';
import { NavLink, useNavigate } from 'react-router-dom';
import ModalTuto from '../base/ModalTuto';
import '../../style/tuto.css'
import '../../style/challenge.css';

function DoAChallenge() {
    const dispatch=useDispatch();
    const dialog = "Les réaliser renforcera ton esprit et il pourra évoluer"
    const [display, setdisplay] = useState(<p></p>);
    const [displayResult, setdisplayResult] = useState(<p></p>);
    const [seeNextStep, setNextStep] = useState(false); 
    let user = useSelector((state: any) => state.userReducer.current_user);

    useEffect(()=>{
        recoverStatus();
    },[])

    function recoverStatus() {
        let order = user.idchallengesFollow;
        axios.put('http://127.0.0.1:8080/challenges/user/'+user.id,  JSON.stringify(order) , { headers: {  "Content-Type": "application/json"  }})
        .then(res => { 
            setdisplay(res.data.map((challenge : any) => <div key={"challenge-"+challenge.chall.id} id={"challenge-"+challenge.chall.id} className={`col-12`}><Challenge challenge={challenge.chall} follow={true} doAnAction={doAChallenge} status={challenge.status}></Challenge></div>)); 
        }); 
    }

    function doAChallenge(challengeChoose: number) {
        let order = {"idchall":challengeChoose,"iduser":user.id};
        axios.post('http://127.0.0.1:8080/challenges/do',  order , { headers: {  "Content-Type": "application/json"  }})
        .then(res => { 
          if ( res.data === "" ) {
            setdisplayResult(<span style={{color:'red'}}>Erreur, challenge déjà réalisé.</span>);
          }
          else {
            user.points = res.data.score;
            user.idchallengesFollow = [];
            dispatch(updateUser(user)); 
            setdisplayResult(<span style={{color:'green'}}>Challenge validé, votre esprit est renforcé</span>);
            setNextStep(true);
          }     
        });
      }

    return(
    <div className="parent">
        <div className="div1"> 
            <div className="bg-radient vh-100">
                <div className="container h-100">
                    <div className="row d-flex justify-content-center align-items-center h-100">
                        <div className="col-12 h-75">
                            <div className="card bg-dark text-white h-100">
                                <div className="card-body text-center">
                                    <div className="pb-2 h-100">
                                        <div className='col-12 h-10 mt-2'>
                                            <h2>Confirmer ton épreuve </h2>
                                        </div>
                                        <div className='col-12 row h-50 mt-3 mb-2'>
                                            {display}
                                        </div>
                                        <div className='col-12'>
                                                {displayResult}
                                            </div>
                                        <div className='col-12'>
                                            { seeNextStep && <NavLink to="/" ><p id="soldPlayer" className='float-right'>Prochaine étape</p></NavLink> }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="div2">
            <ModalTuto dialog={dialog}></ModalTuto>
        </div>
    </div>
    )    
}

export default DoAChallenge;