import React from 'react';
import logo from './logo.svg';
import './App.css';
import { useEffect } from 'react';
import axios from 'axios';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import globalReducer from './reducers';
import MainRouter from './main-app/main-router/MainRouter';

function App() {
  const store = createStore(globalReducer);
  useEffect(()=> {
    axios.defaults.headers.post["Access-Control-Allow-Origin"] = "*";
    axios.defaults.headers.get["Access-Control-Allow-Origin"] = "*";
  },[])

  return (
    <Provider store={store}>
          <MainRouter></MainRouter>
    </Provider>
  );
}

export default App;
