export const updateUser =
    (user) => {
        return { 
            type: 'UPDATE_USER_ACTION', 
            user: user 
        };
    }

export const updateSpirit =
    (spirit) => {
        return { 
            type: 'UPDATE_SPIRIT_ACTION', 
            spirit: spirit 
        };
    }
