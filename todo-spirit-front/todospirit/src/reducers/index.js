import { combineReducers } from 'redux';
import spiritReducer from './spiritReducer';
import userReducer from './userReducer';

const globalReducer = combineReducers({
    userReducer: userReducer,
    spiritReducer:spiritReducer,
});

export default globalReducer;
